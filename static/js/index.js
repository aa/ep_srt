/* Include the Security module, we will use this later to escape a HTML attribute*/
/* mm: removed .js */
var Security = require('ep_etherpad-lite/static/js/security'); 

/* Define the regular expressions we will use to detect if a string looks like a reference to a pad IE [[foo]] */
// var timecodeRegexp = new RegExp(/\{\{([^\]]+)\}\}/g);
var timecodeRegexp = new RegExp(/((?:\d\d\:)?\d\d:\d\d(?:\.\d\d\d)?) -->/g);
// (?:\:\d\d)?

// Define a regexp to detect if we are in timeslider
// var timesliderRegexp = new RegExp(/p\/[^\/]*\/timeslider/g);

/* Take the string and remove the first and last 2 characters IE [[foo]] returns foo */
var linkSanitizingFn = function(result){
  if(!result) return result;
  // result.index = result.index + 2; // old [[foo]] code
  var s = result[0];  
  result[0] = s
    .substr(0,s.length-4); // Skip the first two chars ([[) and omit the last ones (]])
    //.replace(/\s+/g, '_'); // Every space will be replaced by an underscore
  return result;
};


/* CustomRegexp provides a wrapper around a RegExp Object which applies a given function to the result of the Regexp
  @param regexp the regexp to be wrapped
  @param sanitizeResultFn the function to be applied to the result. 
*/
var CustomRegexp = function(regexp, sanitizeResultFn){
  this.regexp = regexp;
  this.sanitizeResultFn = sanitizeResultFn;
};


CustomRegexp.prototype.exec = function(text){
  var result = this.regexp.exec(text); 
  return this.sanitizeResultFn(result);
}


/* getCustomRegexpFilter returns a linestylefilter compatible filter for a CustomRegexp
  @param customRegexp the CustomRegexp Object
  @param tag the tag to be filtered
  @param linestylefilter reference to linestylefilter module
*/
var getCustomRegexpFilter = function(customRegexp, tag, linestylefilter)
{
  var filter =  linestylefilter.getRegexpFilter(customRegexp, tag);
  return filter;
}


exports.aceCreateDomLine = function(name, context){
  var timecode;
  var cls = context.cls;
  var domline = context.domline;
  
  // TODO find a more elegant way.
  //var inTimeslider = (timesliderRegexp.exec(document.location.href) !== null);
  
  if (cls.indexOf('timecode') >= 0) // if it already has the class of internalHref
  {
    cls = cls.replace(/(^| )timecode:(\S+)/g, function(x0, space, tc)
    {
      timecode = tc;
      // return space + "url"; // linkify left the url class
      return space + "timecode";
    });
  }
  
  if (timecode)
  {
    // var url = "#t="+timecode;
    var url = timecode;
    var modifier = {
      extraOpenTags: '<span data-timecode="' + Security.escapeHTMLAttribute(url) +'">',
      extraCloseTags: '</span>',
      cls: cls
    }
    return [modifier];
  }
  return;
}

exports.aceGetFilterStack = function(name, context){
  var linestylefilter = context.linestylefilter;
  var filter = getCustomRegexpFilter(
    new CustomRegexp(timecodeRegexp, linkSanitizingFn),
    'timecode',
    linestylefilter
  );
  return [filter];
}

exports.postAceInit = function (name, context) {
  // console.log("ep_srt: postAceInit", context);
  // use context.ace to locate the iframe to process clicks
  var f = context.ace.getFrame().contentDocument.querySelector("iframe");
  console.log("ep_srt: postAceInit, frame", context);
  f.contentDocument.addEventListener("click", function (e) {
    // console.log("ep_srt.click", e.target.getAttribute("href"));
    if ((e.target.nodeName == "SPAN") && (e.target.getAttribute("data-timecode"))) {
      console.log("ep_srt.click timecode", e.target.getAttribute("data-timecode"));
      window.top.postMessage({msg: "timecodeclick", timecode: e.target.getAttribute("data-timecode")}, "*");
    }
  })
  window.addEventListener("message", function (e) {
    console.log("ep_srt: window.message", e);
    if (e.data.msg == "gettimecoderesponse") {
      console.log("gettimecoderesponse", e.data.timecode, context);
      // window.open(e.data.href, "padlinkclick", '');
      context.ace.replaceRange(e.data.selStart, e.data.selEnd, e.data.timecode+" -->\n");
    }
  }, false);
}

exports.aceEditorCSS = function () {
  return ["ep_srt/static/css/srt.css"]
}

exports.documentReady = function () {
  console.log("ep_srt.documentReady, window:", window);
  window.addEventListener("message", function (e) {
    console.log("ep_srt: window.message", e);
    if (e.data.msg == "padlinkclick") {
      console.log("ep_srt recv padlinkclick", e.data.href);
      window.open(e.data.href, "padlinkclick", '');
    }
  }, false);
}

exports.aceKeyEvent = function (name, context) {
  // console.log("aceKeyEvent", context);
  if (context.evt.type == "keydown" && context.evt.key == "ArrowDown" && context.evt.altKey) {
    var ei = context.editorInfo
    // console.log("ALT-DOWN: PASTETIMECODE", context, context.rep.selStart, context.rep.selEnd);
    window.top.postMessage({msg: "gettimecode", selStart: context.rep.selStart, selEnd: context.rep.selEnd}, "*");
    // ei.ace_importText("TIMECODE", true, false); // text, undoable, dontprocess see ace2_inner.js:679
    // !(rep.selStart && rep.selEnd)
    
    // this link works!!!!
    // ei.ace_replaceRange(context.rep.selStart, context.rep.selEnd, "00:00 -->\n");
    
    // ei.ace_callWithAce(function (ace) {
    //   console.log("ace", ace);
    // });
    return true;
  }
  return false;
}


/*  

 context.ace.callWithAce(function(ace){
        ace.ace_doInsertHeading(intValue);
      },'insertheading' , true);

  */
